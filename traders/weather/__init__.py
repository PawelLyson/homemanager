import urllib3
import json
import time
import threading
import screens.console.consoleInfo
logger = screens.console.consoleInfo.createLogger(__name__)


class WeatherTrader(object):
    def __init__(self):
        self.apiKey = "x6SIK9oivWdkDjAD6Y43FuobNCbeVMTL"
        self.placeNumber = "275781"
        self.link = "http://dataservice.accuweather.com/currentconditions/v1/" + self.placeNumber + "?apikey=" + self.apiKey + "&language=pl-pl&details=true"
        self.httpClient = urllib3.PoolManager()
        self.weatherInfoDict = None
        self.isRunning = True
        self.waitForRefresh = 3600
        self.refreshingThread = threading.Thread(target=self.refreshingData)
        self.startThread()

    def refreshingData(self):
        while self.isRunning:
            response = self.httpClient.request("GET", self.link)
            self.weatherInfoDict = json.loads(response.data.decode("UTF-8"))[0]
            for i in range(self.waitForRefresh):
                if not self.isRunning:
                    break
                time.sleep(1)

    def startThread(self):
        self.isRunning = True
        self.refreshingThread.start()
        time.sleep(5)

    def stopThread(self):
        self.isRunning = False

    def getTemperatureInC(self):
        """[T]Temeratura_w_stopniach C"""
        return self.weatherInfoDict["Temperature"]["Metric"]["Value"]

    def getWeatherInfo(self):
        """[T]Opis_pogody"""
        return self.weatherInfoDict["WeatherText"]

    def getWindDirection(self):
        """[T]Kierunek_wiatru"""
        return self.weatherInfoDict["Wind"]["Direction"]["Localized"]

    def getWindSpeed(self):
        """[T]Predkosc_wiatru"""
        return self.weatherInfoDict["Wind"]["Speed"]["Metric"]["Value"]

    def getHumidity(self):
        """[T]Wilgotnosc_powietrza"""
        return self.weatherInfoDict["RelativeHumidity"]


class Load(WeatherTrader):
    """Pogoda"""
    def __init__(self):
        logger.debug("Uruchomiono moduł "+ str(self.__doc__))
        super().__init__()
        self.methodList = [
            self.getTemperatureInC, self.getWeatherInfo, self.getWindDirection, self.getWindSpeed, self.getHumidity
        ]

    def __str__(self):
        return "Moduł " + str(self.__doc__)

    def start(self):
        self.start()

    def stop(self):
        self.stopThread()

    def getMethodsList(self):
        return self.methodList

    def getMethod(self, number):
        return self.methodList[number]

    def getMethodInfo(self, number):
        return self.methodList[number].__doc__

    def getAmountMethods(self):
        return len(self.methodList)
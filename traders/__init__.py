import importlib
import screens.console.consoleInfo
logger = screens.console.consoleInfo.createLogger(__name__ + "traders")

class importAll(object):
    def __init__(self):
        self.moduleNames = self.loadModulesNames()
        self.loadedModules = self.importModules()

    def loadModulesNames(self):
        return ["time", "weather"]

    def importModules(self):
        loaded = []
        for oneModule in self.moduleNames:
            moduleImport = importlib.import_module("traders." + oneModule)
            loaded.append(moduleImport.Load())
        return loaded

    def stopAll(self):
        logger.debug("Zatrzymano wszystkie moduly")
        for oneModule in self.loadedModules:
            oneModule.stop()

    def getModuleList(self):
        return self.loadedModules

    def getModule(self, number):
        return self.loadedModules[number]

    def getModuleName(self, number):
        return self.loadedModules[number].__doc__

    def getModuleByName(self, name):
        for oneModule in self.loadedModules:
            if oneModule.__doc__ == name:
                return oneModule
        return None

    def getMethodByName(self, name):
        for oneModule in self.loadedModules:
            for oneMethod in oneModule.getMethodsList():
                if oneMethod.__doc__ == name:
                    return oneMethod
        return None

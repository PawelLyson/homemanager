import time
import screens.console.consoleInfo
logger = screens.console.consoleInfo.createLogger(__name__)

polishWeekdayNames = ("Niedziela", "Poniedzialek", "Wtorek", "Sroda", "Czwartek", "Piatek", "Sobota")
polishMonthNames = ("Styczen", "Luty", "Marzec", "Kwiecen", "Maj", "Czerwiec", "Lipiec", "Sierpien", "Wrzesien", "Pazdziernik", "Listopad", "Grudzien")


class TimeTrader(object):
    def getTime(self):
        """[T]Pelna_godzina"""
        return time.strftime("%H:%M:%S")

    def getDate(self):
        """[T]Pelna_data"""
        return time.strftime("%d.%m.%y")

    def getWeekday(self):
        """[T]Dzien_tygodnia"""
        return int(time.strftime("%w"))

    def getPolishWeekday(self):
        """[T]Dzien_tygodnia_opisowo"""
        return polishWeekdayNames[self.getWeekday()]

    def getMonth(self):
        """[T]Aktualny_miesiac"""
        return int(time.strftime("%m"))

    def getPolishMonth(self):
        """[T]Aktualny_miesiac_opisowo"""
        return polishMonthNames[self.getMonth()-1]

    def getDayOfYear(self):
        """[T]Aktualny_dzien_roku"""
        return int(time.strftime("%j"))

    def getWeekOfYear(self):
        """[T]Aktualny_tydzien_roku"""
        return int(time.strftime("%W"))

class Load(TimeTrader):
    """Czas"""
    def __init__(self):
        logger.debug("Uruchomiono moduł "+ str(self.__doc__))
        super().__init__()
        self.methodList = [
            self.getTime, self.getDate, self.getWeekday, self.getPolishWeekday,
            self.getMonth, self.getPolishMonth, self.getDayOfYear, self.getWeekOfYear
        ]

    def __str__(self):
        return "Moduł " + str(self.__doc__)

    def start(self):
        pass

    def stop(self):
        pass

    def getMethodsList(self):
        return self.methodList

    def getMethod(self, number):
        return self.methodList[number]

    def getMethodInfo(self, number):
        return self.methodList[number].__doc__

    def getAmountMethods(self):
        return len(self.methodList)

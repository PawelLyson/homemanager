import time
import threading
import pickle
import os

import screens
import traders
import userInterface.web
import scheduler
import screens.console.consoleInfo

logger = screens.console.consoleInfo.createLogger(__name__)

mainDirectory = os.path.dirname(os.path.realpath(__file__))


class Launcher(object):
    def __init__(self):
        logger.info("Uruchamianie programu")
        logger.info("Prosze czekac...")
        self.isRunning = True
        self.schedulerWorking = scheduler.Scheduler()
        self.functionList = scheduler.FunctionCollection()
        self.loadedModules = traders.importAll()
        self.screenSetsThread = threading.Thread(target=self.runScreenSets)
        self.outScreen = screens.OutsideScreen()
        lcdScreensMethods = (self.getSetsOfScreens, self.getLoadedModules, self.updateScreensData, self.closeProgram,
                             self.outScreen.backlightToggle)
        schedulerMethods = (self.schedulerWorking.getTaskListToSave, self.schedulerWorking.removeTasksByUUID)
        elementsForScreens = (lcdScreensMethods, schedulerMethods)
        self.webServer = userInterface.web.StartWebServer(elementsForScreens)
        self.timeChangeScreen = 5
        self.setsOfScreens = []
        self.dataFileName = "screens.data"
        self.dataFilePath = mainDirectory + "/" + self.dataFileName
        self.runProgram()
        #self.newTask()

    def runProgram(self):
        self.isRunning = True
        self.loadFunctionList()
        self.outScreen.startScreen()
        self.schedulerWorking.start()
        self.screenSetsThread.start()
        time.sleep(5)
        self.loadScreensData()
        self.webServer.start()

    def newTask(self):
        self.schedulerWorking.newTask("Przełączenie podświetlenia2", 0, (45,), "screens_backlightToggle", ())
        pass

    def loadFunctionList(self):
        screensFunctionList = self.outScreen.getListMethods()
        self.functionList.addNewFunction("screens_backlightToggle",
                                         "Przełączenie podświetlenia LCD",
                                         screensFunctionList[0],
                                         "None",
                                         ())
        self.functionList.addNewFunction("screens_setBacklight",
                                         "Ustawienie podświetlenia LCD",
                                         screensFunctionList[1],
                                         "None",
                                         ("Włączyć",))
        self.functionList.addNewFunction("screens_isBacklightON",
                                         "Informacja czy ekran jest podświetlony",
                                         screensFunctionList[0],
                                         "Bool",
                                         ())
        self.functionList.addNewFunction("launcher_shutdown",
                                         "Wyłączenie programu",
                                         self.closeProgram,
                                         "None",
                                         ())

    def closeProgram(self):
        logger.info("Wylaczenie programu")
        time.sleep(2)
        self.isRunning = False
        self.schedulerWorking.stop()
        self.loadedModules.stopAll()
        self.outScreen.stopScreen()
        self.saveScreensData()
        self.webServer.stop()

    def runScreenSets(self):
        while self.isRunning:
            if self.convertNamesToValuesInArray(self.setsOfScreens):
                for screen in self.convertNamesToValuesInArray(self.setsOfScreens):
                    self.outScreen.setAllLines(screen)
                    time.sleep(self.timeChangeScreen)
                    if not self.isRunning:
                        break
            else:
                self.outScreen.setOneLine("Ladowanie...", 0)
            time.sleep(3)

    def loadScreensData(self):
        fileToRead = open(self.dataFilePath, "rb")
        self.setsOfScreens = pickle.load(fileToRead)
        fileToRead.close()

    def convertNamesToValuesInArray(self, arrayToConvert):
        result = []
        if isinstance(arrayToConvert, (tuple, list)):
            for oneElement in arrayToConvert:
                result.append(self.convertNamesToValuesInArray(oneElement))
        else:
            elementResult = self.loadedModules.getMethodByName(arrayToConvert)
            if elementResult is not None:
                return elementResult
            else:
                return arrayToConvert
        return result

    def saveScreensData(self):
        fileToSave = open(self.dataFilePath, "wb")
        pickle.dump(self.setsOfScreens, fileToSave)
        fileToSave.close()

    def updateScreensData(self, screensData):
        self.setsOfScreens = screensData
        self.saveScreensData()

    def getLoadedModules(self):
        return self.loadedModules

    def getSetsOfScreens(self):
        return self.setsOfScreens


software = Launcher()
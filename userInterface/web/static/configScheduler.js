var schedulerTasks = null;


class SchedulerList{
    constructor(){
        this.parentDiv = $('#taskList');
    }

    createListTask(listAllTask){
        var that = this
        $.each(listAllTask, function( index, value ) {
            that.parentDiv.append(that.createOneTask(value, that));
          });
    }

    createOneTask(oneTask, sendedThis){
        var that = sendedThis;
        var name = oneTask[0];
        var UUID = oneTask[1];
        var typeTask = oneTask[2];
        var timeActivate = oneTask[3];
        var methodTask = oneTask[4];
        var argumentsTask = oneTask[5];
        var resultDiv = $('<div id="' + UUID + '" class="oneTask"></div>');
        resultDiv.append($('<div class="titleTask">' + name + '<div>'));
        resultDiv.append($('<div class="typeTask">' + this.typeToDescString(typeTask) + '<div>'));
        resultDiv.append($('<div class="time">' + this.timeActivateConvert(timeActivate) + '<div>'));
        resultDiv.append($('<div class="methodTask">' + methodTask + '<div>'));
        resultDiv.append($('<div class="methodTask">' + methodTask + '<div>'));//argumenty
        var buttonRemove = $('<button id="removeButton">Usuń</button>');
        buttonRemove.on("click", {UUID:UUID,sourceThis: that}, that.removeTask);
        resultDiv.append(buttonRemove);
        return resultDiv;
    }

    removeTask(event){
        var that = event.data.sourceThis;
        that.getDataFromServer('removeTask',{uuid: event.data.UUID,} ,false);
    }

    timeActivateConvert(timeActive){
        var result = "";
        for (var i=0; i < timeActive.length; i++){
            var number = timeActive[i];
            var zero = number < 10 ? "0": ""; 
            result += zero + number + ":";
        }
        return result.slice(0,-1);
    }

    typeToDescString(typeTask){
        if (typeTask == 0){
            return "Cykliczne";
        }
        else
            return "Jednorazowe";
    }

    getDataFromServer(urlToSend,sendData, isReturnData = true){
    var that = this;
    var result = $.ajax({
        type     : "GET",
        url      : urlToSend,
        data     : sendData,
        error: function(jqXHR, errorText, errorThrown) {
            alert("error\n"+ errorText);
        }
    });
    if (isReturnData){
        result.done(function( data, textStatus, jqXHR ) {
            that.returnedData(data);
            console.log(sendData , "Returned")
        });
    }
    console.log(sendData ," Sended")
    return result;
    }   

    returnedData(data){
        console.log(data);
        this.createListTask(data);
    }
}

class CreateTask{
    constructor(){
        this.parentDiv = $('#createTask');
    }
    create(){
        var result = $('<form class="task"></form>');
        var name = $('<div id="name"></div>');
        name.append("Nazwa zadania: ")
        name.append($('<input type="text" name="name">'));
        result.append(name);

        var typeTask = $('<div id="type"></div>');
        typeTask.append('<input type="radio" name="typeTask" value="0" checked> Cykliczne ');
        typeTask.append('<input type="radio" name="typeTask" value="1"> Jednorazowe ');
        result.append(typeTask);

        var functionName = $('<div id="function"></div>');
        var functionSelect = $('<select name="functionName"></select>');
        functionSelect.append('<option value="Test">test</option>');
        functionName.append(functionSelect);
        result.append(functionName);

        var functionsArguments = $('<div id="arguments"></div>');
        functionsArguments.append("argumenty: <br>");
        functionsArguments.append($('<textarea name="arguments"></textarea>'));
        result.append(functionsArguments);


        this.parentDiv.append(result);
    }
}


$(function(){
    var list = new SchedulerList();
    var createTaskForm = new CreateTask();
    createTaskForm.create();

    $('#testButton').on('click', {scheduler: list,}, loadData);
});

function loadData(event){
    event.data.scheduler.getDataFromServer("submitConfigScheduler",{result: "scheduler",}, true);
}

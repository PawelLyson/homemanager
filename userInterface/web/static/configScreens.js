var actualScreens = [["Pierwsza linia",["Godzina  ","[T]Pelna_godzina"],"Test","20170725"],["[T]Pelna_godzina",["[T]Dzien tygodnia opisowo ","[T]Dzien_tygodnia opisowo"],"Trzy","SIEDEM"]];
var allModules = [[["[T]Pelna_godzina","23:48:19"],["[T]Pelna_data","11.08.17"],["[T]Dzien_tygodnia","5"],["[T]Dzien_tygodnia opisowo","Piatek"],["[T]Godzina_i_data","23:48:19    11.08.17"],["[T]Aktualny_miesiac","8"],["[T]Aktualny_miesiac_opisowo","Sierpien"],["[T]Aktualny_dzien_roku","223"],["[T]Aktualny_tydzien_roku","32"],["[T]Informacje_o_dniu_i_tygodniu roku","Dzien 223 Tydzien 32"]],[["[T]Temeratura_w_stopniach C","20.0"],["[T]Opis_pogody","Bezchmurnie"],["[T]Kierunek_wiatru","WNW"],["[T]Predkosc_wiatru","11.1"],["[T]Wilgotnosc_powietrza","82"]]];
var modulesNames = ["time", "weather"];

class MovableLists{
    constructor(parentDiv, cssName, sourceData, functionRefreshData){
        this.parentDiv = parentDiv;
        this.cssName = cssName;
        this.sourceData = sourceData;
        this.functionRefreshDataRefreshData;
        this.recreate();
    }
    create(){};
    recreate(){this.create()};

    createButtonHtml(idString, visibleString, functionToRun = null, that = null){
        let buttonElement = $('<button class="ui-button ui-widget ui-corner-all" id="' + idString + '">'+ visibleString +' </button>');
        if (functionToRun != null){
            buttonElement.on("click", { this: that,} ,functionToRun);
        }
        return buttonElement;
    }

    createEmptyList(moduleNumber = "", css = this.cssName, title = null, parentDiv = this.parentDiv){
        if (title != null){
                parentDiv.append('<div class="titleList title'+ css +'">' + title + '</div>');
            }
        parentDiv.append('<ul id="sortable1" class="sortable connectedSortableModules '+ css + moduleNumber + '"></ul>');
    }

    createHeader(stringToSet){
        this.parentDiv.append('<div class="header">' + stringToSet + '</div>');
    }

    addMiniHeader(stringToSet){
        this.parentDiv.append('<div class="miniheader">' + stringToSet + '</div>');
    }

    dataToString(data){
        var that = this;
        if (data instanceof Array)
        {
            var result = ""
            $.each(data, function(key, element){
                result += that.getValueFromModuleName(element)
            });
            return result;
        }
        return that.getValueFromModuleName(data)
    }

    getValueFromModuleName(value){
        for (var i=0; i<allModules.length; i+=1){
            for(var j=0; j<allModules[i].length; j+=1){
                if (allModules[i][j][0] == value){
                    return allModules[i][j][1];
                }
            }
        }
        return value;
    }

    createHtmlLiElement(first, second){
        return '<li class="ui-state-highlight"><pre><div id="data">'+ first +'</div><div id="value" class="marquee">'+ second +'</div></pre></li>'
    }

    connectUlList() {
        this.parentDiv.children("ul").sortable({
            connectWith: ".sortable",
            revert: "invalid"
        }).disableSelection();
        $( ".checkboxes" ).checkboxradio({
            icon: false
        });
    }

    removeChilds(parentElement){
        parentElement.children().remove();
    }

    createMultiLineFromArray(elementArray){
        var resultToShow = "";
        var that = this;
        $.each(elementArray, function(number, elementArray){
            resultToShow += that.createHtmlLiElement(elementArray, that.dataToString(elementArray));
        });
        return resultToShow;
    }

    createOneLineFromArray(elementArray){
        var resultToShow = "";
        var that = this;
        $.each(elementArray, function(number, element){
            resultToShow += that.dataToString(element) +" ";
        });
        var dataToShow = "[" + elementArray.toString() + "]";
        return this.createHtmlLiElement(dataToShow, resultToShow);
    }
    
    getDataFromServer(urlToSend,sendData, isReturnData = true){
    var that = this;
    var result = $.ajax({
        type     : "GET",
        url      : urlToSend,
        data     : sendData,
        error: function(jqXHR, errorText, errorThrown) {
            alert("error\n"+ errorText);
        }
    });
    if (isReturnData){
        result.done(function( data, textStatus, jqXHR ) {
            that.returnedData(data);
            console.log(sendData , "Returned")
        });
    }
    console.log(sendData ," Sended")
    return result;
    }
}

class ModulesList extends MovableLists{
    create(){
        this.removeChilds(this.parentDiv);
        this.createHeader("Moduły");
        var that = this;
        var i = 0;
        $.each(this.sourceData, function(moduleNumber, oneModule){
            that.createEmptyList(moduleNumber, that.cssName, "Moduł " + modulesNames[i]);
            var parentUl = that.parentDiv.children('.' + that.cssName + moduleNumber);
            $.each(oneModule, function(elementNumber,oneElement){
                parentUl.append(that.createHtmlLiElement(oneElement[0], oneElement[1]));
            });
            i++;
        });
        this.connectUlList();
    }
    recreate()
    {
        this.getDataFromServer("submitConfigScreens",{result : "moduly",});
    }

    returnedData(data){
        //this.modulesNames = getDataFromServer("submitConfigScreens",{result :"nazwy",});
        this.sourceData = data;
        allModules = data;
        this.create()
    }
}

class ScreensList extends MovableLists{
    create(){ 
        this.removeChilds(this.parentDiv);
        this.createHeader("Ekrany");
        var that = this;
        $.each(this.sourceData, function(moduleNumber, oneModule){
            that.createEmptyListWithCheckbox(moduleNumber, that.cssName, "Ekran numer "+ (moduleNumber+1));
            var parentUl = that.parentDiv.children('.' + that.cssName + moduleNumber);
            $.each(oneModule, function(lineNumber,line){
                var dataString = line
                if (line instanceof Array){
                    dataString = "[" + line + "]"
                }
                parentUl.append(that.createHtmlLiElement(dataString, that.dataToString(line)));
            });
        });
        this.createExtraButtons();

    }  
    recreate()
    {
        this.getDataFromServer("submitConfigScreens",{result : "ekran",});
    }

    createExtraButtons(){
        var motherDiv = $('<div class="extraButtons"></div>');
        var addListButtonWithDiv = $('<div class="addListButton"></div>');
        addListButtonWithDiv.append(this.createButtonHtml("createEmptyListButton","Stwórz nową liste",this.addNewEmptyList, this));
        motherDiv.append(addListButtonWithDiv);
        var saveButtonWithDiv = $('<div class="resultDivButton"></div>');
        saveButtonWithDiv.append(this.createButtonHtml("screensButtonSend","Zapisz",this.setScreens, this));
        motherDiv.append(saveButtonWithDiv);
        this.parentDiv.append(motherDiv);
    }

    removeExtraButtons(){
        this.parentDiv.children('.extraButtons').remove();
    }

    returnedData(data){
        //this.modulesNames = getDataFromServer("submitConfigScreens",{result :"nazwy",});
        this.sourceData = data;
        actualScreens = data;
        this.create()
    }

    createEmptyListWithCheckbox(moduleNumber = "", css = this.cssName, title = null){
        this.createEmptyList(moduleNumber, css, title);
        var checkbox = $('<label class="removeScreenButton" for="checkbox-nested-' + moduleNumber + '">Usuń <input type="checkbox" class="checkboxes" name="checkbox-nested-' + moduleNumber + '" id="checkbox-nested-' + moduleNumber + '"></label>');
        this.parentDiv.children(".titleList").last().append(checkbox);
        this.connectUlList();
    }

    setScreens(event){
        var that = event.data.this;
        var result = [];
        for (var i = 0;i<that.parentDiv.children("ul").length; i+=1){
            if  (!$('#checkbox-nested-'+i ).is(':checked')){
                result.push([]);
                for (var j = 1;j<=4; j+=1){
                    var oneLine = $('.screen'+ i +' li:nth-child('+ j +') #data')[0].outerText;
                    if (oneLine.startsWith("[") && oneLine.endsWith("]")){
                        oneLine = oneLine.substring(1, oneLine.length - 1);
                        oneLine = oneLine.split(",");
                    }
                    result[result.length-1].push(oneLine);
                }
            }
        }
        that.getDataFromServer("change",{screens: JSON.stringify(result),}, false);
    }
    addNewEmptyList(event){
        var that = event.data.this;
        var moduleNumber = that.parentDiv.children("ul").length;
        that.removeExtraButtons();
        that.createEmptyListWithCheckbox(moduleNumber ,that.cssName, "Ekran numer "+ (moduleNumber +1));
        that.createExtraButtons();
    }
}

class WorkShopList extends MovableLists{
    create(){
        this.createHeader("Warsztat");
        this.createEmptyList("","workshopSource", "Źródło");
        this.createButtonsWorkplace();
        this.createEmptyList("", "finalplace", "Wynik");
        this.connectUlList();
    }    

    createButtonsWorkplace(){
        var buttonsDiv = $('<div class="buttonsDiv workshopButtons"></div>');
        buttonsDiv.append(this.createButtonHtml("mergeButton", "Połącz", this.mergeButtonRun, this));
        buttonsDiv.append(this.createButtonHtml("splitButton", "Rozłącz", this.splitButtonRun, this));
        buttonsDiv.append(this.createButtonHtml("copyButton", "Skopiuj", this.copyButtonRun, this));
        this.parentDiv.append(buttonsDiv);
    }

    copyButtonRun(event){
        var that = event.data.this;
        var elementsToCopy = $(".workshopSource").children("*").clone();
        var resultList = $('.finalplace');
        that.removeChilds(resultList);
        resultList.append(elementsToCopy);
    }

    splitButtonRun(event){
        var result = [];
        var that = event.data.this;
        var liElements = $(".workshopSource");
        for (var j = 1;j<=liElements.children("li").length; j+=1){
            var oneLine = liElements.find('li:nth-child('+ j +') > pre > #data')[0].outerText;
            if (oneLine.startsWith("[") && oneLine.endsWith("]")){
                oneLine = oneLine.substring(1, oneLine.length - 1);
                oneLine = oneLine.split(",");
            }
            result = result.concat(oneLine);
        }
        var resultList = $('.finalplace');
        that.removeChilds(liElements);
        that.removeChilds(resultList);
        resultList.append(that.createMultiLineFromArray(result));
    }

    mergeButtonRun(event){
        var result = [];
        var that = event.data.this;
        var liElements = $(".workshopSource");
        for (var j = 1;j<=liElements.children("li").length; j+=1){
            var oneLine = liElements.find('li:nth-child('+ j +') > pre > #data')[0].outerText;
            if (oneLine.startsWith("[") && oneLine.endsWith("]")){
                oneLine = oneLine.substring(1, oneLine.length - 1);
                oneLine = oneLine.split(",");
                $.each(oneLine, function(number, element){
                    result.push(element);
                });
                continue;
            }
            result.push(oneLine);
        }
        var resultList = $('.finalplace');
        that.removeChilds(liElements);
        that.removeChilds(resultList);
        resultList.append(that.createOneLineFromArray(result));
    }
}

class ConverterList extends MovableLists{
    create(){
        this.createHeader("Konwerter");
        var buttonsDiv = $('<div class="buttonsDiv converterButtons"></div>');
        buttonsDiv.append('<label for="stringToConvert">Tekst: </label> <input id="stringToConvert">');
        buttonsDiv.append(this.createButtonHtml("convertButton", "Zamień", this.convertButtonRun, this));
        this.parentDiv.append(buttonsDiv);
        this.createEmptyList("","converterResult");
        this.connectUlList();
    }

    convertButtonRun(event){
        var that = event.data.this;
        var stringElement = $("#stringToConvert").val();
        var resultList = $(".converterResult");
        that.removeChilds(resultList);
        resultList.append(that.createHtmlLiElement(stringElement, stringElement));
    }
}

$(function(){
    var modulesDiv = $(".modules");
    var modules = new ModulesList(modulesDiv, "module", allModules, function(){return null});
    var screensDiv = $(".screens");
    var screens = new ScreensList(screensDiv, "screen", actualScreens, function(){return null});
    var workshopDiv = $(".workshop");
    var workshop = new WorkShopList(workshopDiv, "screen", null, function(){return null});
    var converterDiv = $(".converter");
    var converter = new ConverterList(converterDiv, "converter", null, function(){return null});

    
    $('#refreshDataButton').on('click', {modules: modules, screens:screens},refreshing)
    $('#shutdownProgram').on('click', {modules: modules,},shutdownProgram)
    $('#toggleBacklight').on('click', {modules: modules,},toggleBacklight)

});

function refreshing(event){
    event.data.modules.recreate();
    event.data.screens.recreate();
}

function shutdownProgram(event){
    event.data.modules.getDataFromServer("submitConfigScreens",{result: "shutdown",},false);
}
function toggleBacklight(event){
    event.data.modules.getDataFromServer("submitConfigScreens",{result: "backlight",},false);
}
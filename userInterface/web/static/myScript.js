var actualScreens = [["Pierwsza linia",["Godzina  ","[T]Pelna_godzina"],"Test","20170725"],["[T]Pelna_godzina",["[T]Dzien tygodnia opisowo ","[T]Dzien_tygodnia opisowo"],"Trzy","SIEDEM"]];
var allModules = [[["[T]Pelna_godzina","23:48:19"],["[T]Pelna_data","11.08.17"],["[T]Dzien_tygodnia","5"],["[T]Dzien_tygodnia opisowo","Piatek"],["[T]Godzina_i_data","23:48:19    11.08.17"],["[T]Aktualny_miesiac","8"],["[T]Aktualny_miesiac_opisowo","Sierpien"],["[T]Aktualny_dzien_roku","223"],["[T]Aktualny_tydzien_roku","32"],["[T]Informacje_o_dniu_i_tygodniu roku","Dzien 223 Tydzien 32"]],[["[T]Temeratura_w_stopniach C","20.0"],["[T]Opis_pogody","Bezchmurnie"],["[T]Kierunek_wiatru","WNW"],["[T]Predkosc_wiatru","11.1"],["[T]Wilgotnosc_powietrza","82"]]];



function showScreens(){
    var screensDiv = $(".screens");
    $.each(actualScreens, function(screenNumber, screen){
        var checkboxName = 'checkbox-nested-' + screenNumber;
        var htmlCode = '<div class="screenNameRemove">Ekran numer ' + screenNumber ;
        htmlCode +='<label for="' + checkboxName + '">Usuń <input type="checkbox" class="removeScreenButton" name="' + checkboxName + '" id="'+ checkboxName +'"></label></div>';
        htmlCode +='<ul id="sortable1" class="sortable connectedSortableScreens screen' + screenNumber + '"></ul>';
        
        screensDiv.append(htmlCode);
        var parentUl = $('.screen' + screenNumber);
        $.each(screen, function(lineNumber,line){
            var dataString = line
            if (line instanceof Array){
                dataString = "[" + line + "]"
            }
            parentUl.append('<li class="ui-state-default"><div id="data">'+ dataString +'</div><div id="value">'+ dataToString(line) +'</div></li>');
        });
    });
}

function showModules(){
    var modulesDiv = $(".modules");
    $.each(allModules, function(moduleNumber, oneModule){
        modulesDiv.append('<h3>Moduł numer ' + moduleNumber + '</h3><ul id="sortable1" class="sortable connectedSortableModules module' + moduleNumber + '"></ul>');
        var parentUl = $('.module' + moduleNumber);
        $.each(oneModule, function(elementNumber,oneElement){
            parentUl.append('<li id="draggable" class="ui-state-highlight moduleSource"><div id="data">'+ oneElement[0] +'</div><div id="value">'+ dataToString(oneElement[0]) +'</div></li>');
        });
    });
}

function createWorkshop(){
    var actualDiv = $(".workshop");
    actualDiv.append('<h3>Warsztat </h3><ul id="sortable1" class="sortable connectedSortableWorkshop workplace"></ul>');
}

function createConverter(){
    var actualDiv = $(".converter");
    actualDiv.append('<h3>Tekst do lini</h3>');
    actualDiv.append('<label for="stringToConvert">Tekst: </label> <input id="stringToConvert">');
    actualDiv.append('<br/><button class="ui-button ui-widget ui-corner-all" id="convertButton">Zamień</button>');
    actualDiv.append('<ul id="sortable1" class="sortable connectedSortableWorkshop converterResult"></ul>');
}

function creatFinalplace(){
    var actualDiv = $(".workshop");
    actualDiv.append('<h3>Przetworzone </h3><ul id="sortable1" class="sortable connectedSortableWorkshop finalplace"></ul>');
}

function createButtonsWorkplace(){
    var actualDiv = $(".workshop");
    actualDiv.append(createButtonHtml("mergeButton","Połącz"));
    actualDiv.append(createButtonHtml("splitButton","Rozłącz"));
    actualDiv.append(createButtonHtml("copyButton","Skopiuj"));
}

function createButtonHtml(idString, visibleString){
    return '<button class="ui-button ui-widget ui-corner-all" id="' + idString + '">'+ visibleString +' </button>'
}
    
function setScreens(){
    var result = [];
    for (var i = 0;i<actualScreens.length; i+=1){
        if  (!$('#checkbox-nested-'+i ).is(':checked')){
            result.push([]);
            for (var j = 1;j<=4; j+=1){
                var oneLine = $('.screen'+ i +' li:nth-child('+ j +') #data')[0].outerText;
                if (oneLine.startsWith("[") && oneLine.endsWith("]")){
                    oneLine = oneLine.substring(1, oneLine.length - 1);
                    oneLine = oneLine.split(",");
                }
                result[result.length-1].push(oneLine);
            }
        }
    }
    console.log(result);
    alert(result);
}

function getValueFromModuleName(value){
    for (var i=0; i<allModules.length; i+=1){
        for(var j=0; j<allModules[i].length; j+=1){
            if (allModules[i][j][0] == value){
                return allModules[i][j][1];
            }
        }
    }
    return value;
}

function dataToString(data){
    if (data instanceof Array)
    {
        result = ""
        $.each(data, function(key, element){
            result += getValueFromModuleName(element)
        });
        return result;
    }
    return getValueFromModuleName(data)
}

// function connectModulesList() {
//     $( "#draggable" ).draggable({
//         connectWith: ".sortable",
//         helper: "clone",
//         revert: "invalid"
//     }).disableSelection();
// }

function connectUlLists() {
    $( ".sortable").sortable({
        connectWith: ".sortable",
        revert: "invalid"
    }).disableSelection();
}
    
function splitButtonRun(){
    var result = [];
    for (var j = 1;j<=$('.workplace li').length; j+=1){
        var oneLine = $('.workplace li:nth-child('+ j +') #data')[0].outerText;
        if (oneLine.startsWith("[") && oneLine.endsWith("]")){
            oneLine = oneLine.substring(1, oneLine.length - 1);
            oneLine = oneLine.split(",");
        }
        result = result.concat(oneLine);
        console.log(result);
    }
    var parentUl = $('.finalplace');
    removeChilds($('.workplace'));
    removeChilds(parentUl);
    parentUl.append(createMultiLineFromArray(result));
    console.log(result);
}

function mergeButtonRun(){
    var result = [];
    for (var j = 1;j<=$('.workplace li').length; j+=1){
        var oneLine = $('.workplace li:nth-child('+ j +') #data')[0].outerText;
        if (oneLine.startsWith("[") && oneLine.endsWith("]")){
            oneLine = oneLine.substring(1, oneLine.length - 1);
            oneLine = oneLine.split(",");
            $.each(oneLine, function(number, element){
                result.push(element);
            });
            continue;
        }
        result.push(oneLine);
    }
    var parentUl = $('.finalplace');
    removeChilds($('.workplace'));
    removeChilds(parentUl);
    parentUl.append(createOneLineFromArray(result));
    console.log(result);
}

function copyButtonRun(){
    elementsToCopy = $('.workplace li').clone();
    var parentUl = $('.finalplace');
    removeChilds(parentUl);
    parentUl.append(elementsToCopy);

}

function convertButtonRun(){
    var stringElement = $("#stringToConvert").val();
    var parentLi = $(".converterResult");
    removeChilds(parentLi);
    parentLi.append('<li class="ui-state-highlight moduleSource"><pre><div id="data">'+ stringElement +'</div><div id="value">'+ stringElement +'</div></pre></li>');
}

function removeChilds(parentElement){
    parentElement.children().remove();
}

function createMultiLineFromArray(elementArray){
    var resultToShow = "";
    $.each(elementArray, function(number, elementArray){
        resultToShow += '<li class="ui-state-highlight moduleSource"><pre><div id="data">'+ elementArray +'</div><div id="value">'+ dataToString(elementArray) +'</div></pre></li>';
    });
    return resultToShow;
}

function createOneLineFromArray(elementArray){
    var resultToShow = "";
    $.each(elementArray, function(number, element){
        resultToShow += dataToString(element) +" ";
    });
    return '<li class="ui-state-highlight moduleSource"><pre><div id="data">['+ elementArray +']</div><div id="value">'+ resultToShow +'</div></pre></li>';
}

$(function(){ //On start 
    showModules();
    showScreens();
    createWorkshop();
    createButtonsWorkplace();
    creatFinalplace();
    createConverter();
    connectUlLists();
    // connectModulesList();
    $("#guzik").on("click", setScreens);
    $("#splitButton").on("click", splitButtonRun);
    $("#mergeButton").on("click", mergeButtonRun);
    $("#copyButton").on("click", copyButtonRun);
    $("#convertButton").on("click", convertButtonRun);
    $("#refreshDataButton").on("click", getModulesFromSerwer);
})

$(function() {
    $( ".removeScreenButton" ).checkboxradio({
      icon: false
    });
  } );

ajaxResult = null;
function getModulesFromSerwer(){
    var tempVar = ajaxFunc("moduly");
    console.log(ajaxResult);
    allModules = ajaxResult;
    removeChilds($(".modules"));
    showModules();
    connectUlLists();
}

function ajaxFunc(toGet){
nameVal = toGet;
$.ajax({
    type     : "GET",
    url      : "submit",
    data     : {
        result : nameVal,
    },
    success: function(jsonRes) {
        //ten fragment wykona się po pomyślnym zakończeniu połączenia - gdy dostaliśmy odpowiedź od serwera nie będącą błędem (404, 400, 500 itp)
        //atrybut ret zawiera dane zwrócone z serwera
        
        // jQuery.each(jsonRes, function(i, ob) {
        //     alert(ob);
        //     alert(i);
        // });

        ajaxResult = jsonRes;
    },
    complete: function() {
        //ten fragment wykona się po zakończeniu łączenia - nie ważne czy wystąpił błąd, czy sukces
        //alert("complete");
    },
    error: function(jqXHR, errorText, errorThrown) {
        //ten fragment wykona się w przypadku BŁĘDU
        //do zmiennej errorText zostanie przekazany błąd
        alert("error\n"+ errorText);
    }
});
}
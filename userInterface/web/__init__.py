import cherrypy
import codecs
import os
import threading
import json
import screens.console.consoleInfo
logger = screens.console.consoleInfo.createLogger(__name__)

webDirectory = os.path.dirname(os.path.realpath(__file__))


class MainSite(object):
    def __init__(self, elementsForScreens):
        lcdScreensMethods, schedulerMethods = elementsForScreens
        self.getSetOfScreens, self.getLoadedModules, self.updateScreensData, self.closeMainProgram, self.screenBacklightToggle = lcdScreensMethods
        self.getTaskListToSave, self.removeTasksByUUID = schedulerMethods

    @cherrypy.expose
    def configScheduler(self):
        return codecs.open(webDirectory + "/configScheduler.html", "r", "utf-8")

    @cherrypy.expose
    def submitConfigScheduler(self, result=""):
        cherrypy.response.headers['Content-Type'] = 'application/json'
        if result == "scheduler":
            return json.dumps(self.getTaskListToSave(), ensure_ascii=False).encode('utf-8')

    @cherrypy.expose
    def index(self):
        return codecs.open(webDirectory + "/begin.html", "r", "utf-8")

    @cherrypy.expose
    def configScreens(self):
        return codecs.open(webDirectory + "/configScreens.html", "r", "utf-8")

    def getArrayAvailableModules(self):
        modulesArray = []
        i = 0
        for module in self.getLoadedModules().getModuleList():
            modulesArray.append([])
            j = 0
            for method in module.getMethodsList():
                modulesArray[i].append([])
                modulesArray[i][j].append(str(method.__doc__))
                modulesArray[i][j].append(str(method()))
                j += 1
            i += 1
        return modulesArray

    @cherrypy.expose
    def change(self, screens):
        arrayToSend = json.loads(screens)

        self.updateScreensData(arrayToSend)
        sendEelement = json.dumps(arrayToSend)
        return sendEelement

    @cherrypy.expose
    def removeTask(self, uuid="pusty"):
        self.removeTasksByUUID(uuid)

    @cherrypy.expose
    def submitConfigScreens(self, result="pusty"):
        cherrypy.response.headers['Content-Type'] = 'application/json'
        if result == "moduly":
            result = self.generateJsArray(self.getArrayAvailableModules())
        elif result == "ekran":
            result = self.generateJsArray(self.getSetOfScreens())
        elif result == "nazwy":
            result = json.dumps(self.getModulesNamesList())
        elif result == "shutdown":
            self.closeMainProgram()
            result = "[]"
        elif result == "backlight":
            self.screenBacklightToggle()
            result = "[]"
        return result.encode('utf-8')

    def getModulesNamesList(self):
        return self.getLoadedModules().loadModulesNames()

    def generateJsArray(self, objToConvert):
        return "[" + self.separateObjectForArray(objToConvert)[:-1] + "]"

    def separateObjectForArray(self, objToConvert):
        result = ""
        if isinstance(objToConvert, (tuple, list)):
            for oneElement in objToConvert:
                if isinstance(oneElement, (tuple, list)):
                    result += "[" + self.separateObjectForArray(oneElement)[:-1] + "],"
                else:
                    result += self.separateObjectForArray(oneElement)
        else:
            if callable(objToConvert):
                return "\"" + str(objToConvert.__doc__) + "\","
            else:
                return "\"" + str(objToConvert) + "\","
        return result


class StartWebServer(object):
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': webDirectory
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './static'}
        }
    cherrypy.config.update({
    'tools.staticdir.debug': True,
    'log.screen': True,
    'tools.sessions.on': True,
    'tools.encode.on': True,
    'tools.encode.encoding': 'utf-8',
    'server.socket_host': '192.168.0.127',
    'server.socket_port': 8080
    })

    def __init__(self, elementsForScreens):
        self.webSites = MainSite(elementsForScreens)
        self.webThread = threading.Thread(target=cherrypy.quickstart, args=[self.webSites, '/', self.conf])

    def stop(self):
        logger.info("Wylaczono serwer WWW")
        cherrypy.engine.exit()

    def start(self):
        logger.info("Uruchomiono serwer WWW")
        self.webThread.start()



import time
import uuid
import pickle
import threading
import collections
import os

import screens.console.consoleInfo
logger = screens.console.consoleInfo.createLogger(__name__)

mainDirectory = os.path.dirname(os.path.realpath(__file__))


class FunctionCollection(object):

    functionsList = []

    def __init__(self):
        pass

    def addNewFunction(self, name, desc, functionToRun, returnValueDesc, argsDesc):
        self.functionsList.append(FunctionWithInfo(name, desc, functionToRun, returnValueDesc, argsDesc))

    def getNameFunctionList(self):
        return list(map(lambda x: x.getName(), self.functionsList))

    def executeFunction(self, name, args=()):
        for element in self.functionsList:
            if element.getName() == name:
                return element.execute(args)

    def getFunctionToExecute(self, name):
        for element in self.functionsList:
            if element.getName() == name:
                return element.execute

    def getFunctionDesc(self, name):
        for element in self.functionsList:
            if element.getName() == name:
                return element.getDesc(), element.getReturnValue(), element.getArgsDesc()

    def getAllFunctionDesc(self):
        result = []
        for element in self.functionsList:
            result.append((element.getName(), element.getDesc(), element.getReturnValue(), element.getArgsDesc()))
        return result

    def getAllFunction(self):
        result = []
        for element in self.functionsList:
            result.append(element)
        return result


class FunctionWithInfo(object):
    def __init__(self, name, desc, functionToRun, returnValueDesc, argsDesc):
        self.name = name
        self.desc = desc
        self.functionToRun = functionToRun
        self.argsDesc = argsDesc
        self.returnValueDesc = returnValueDesc

    def getName(self):
        return self.name

    def getDesc(self):
        return self.desc

    def execute(self, args):
        return self.functionToRun(*args)

    def getReturnValue(self):
        return self.returnValueDesc

    def getArgsDesc(self):
        return self.argsDesc


class Scheduler(object):
    def __init__(self):
        logger.debug("Zainicjowano harmonogram")
        self.runningTaskList = []
        self.isRunning = False
        self.runningThread = threading.Thread(target=self.running)
        self.lock = threading.Lock()
        self.dataFileName = "scheduler.data"
        self.dataFilePath = mainDirectory + "/" + self.dataFileName

    def start(self):
        logger.info("Uruchomiono harmonogram")
        self.isRunning = True
        self.loadData()
        self.runningThread.start()

    def stop(self):
        logger.info("Wylaczono harmonogram")
        self.saveData()
        self.isRunning = False

    def newTask(self, taskName, taskType, executionTime, nameFunctionToRun, functionArguments):
        logger.info("Dodano nowe zadanie do harmonogramu: " + taskName)
        createdUUID = str(uuid.uuid4())
        task = Task(taskName, createdUUID, taskType, executionTime, nameFunctionToRun, functionArguments)
        self.runningTaskList.append(RunningTask(task))
        return createdUUID

    def getTaskListToSave(self):
        result = []
        for element in self.runningTaskList:
            root = element.rootObject
            result.append((root.name, root.taskUUID, root.type, root.executionTime, root.functionToRun, root.functionArguments))
        print(result)
        return result

    def saveData(self):
        dataToSave = []
        for oneTask in self.runningTaskList:
            dataToSave.append(oneTask.rootObject)
        fileToSave = open(self.dataFilePath, "wb")
        pickle.dump(dataToSave, fileToSave)
        fileToSave.close()

    def loadData(self):
        fileToRead = open(self.dataFilePath, "rb")
        loadedFile = pickle.load(fileToRead)
        fileToRead.close()
        for oneTask in loadedFile:
            self.runningTaskList.append(RunningTask(oneTask))

    def removeTasksByName(self, name):
        logger.info("Usunieto zadanie o nazwie: " + name)
        self.lock.acquire()
        for task in self.runningTaskList:
            if task.getName() == name:
                self.runningTaskList.remove(task)
        self.lock.release()

    def removeTasksByUUID(self, taskUUID):
        logger.info("Usunieto zadanie o UUID: " + taskUUID)
        self.lock.acquire()
        for task in self.runningTaskList:
            if task.getTaskUUID() == taskUUID:
                self.runningTaskList.remove(task)
        self.lock.release()

    def removeObsoleteTasks(self):
        self.lock.acquire()
        for task in self.runningTaskList:
            if task.isToRemove():
                logger.info("Usunięto przedawnione zadanie: " + task.getName())
                self.runningTaskList.remove(task)
        self.lock.release()

    def getAllInfoAsList(self):
        result = []
        for task in self.runningTaskList:
            result.append([task.getTaskUUID(), task.getName(), task.type])
        return result

    def running(self):
        while self.isRunning:
            self.removeObsoleteTasks()
            self.lock.acquire()
            for task in self.runningTaskList:
                task.execute()
            self.lock.release()
            time.sleep(0.5)

class Task(object):
    def __init__(self, taskName, taskUUID, taskType, executionTime, functionToRun, functionArguments):
        self.name = taskName
        self.taskUUID = taskUUID
        self.type = taskType
        self.executionTime = executionTime
        self.functionToRun = functionToRun
        self.functionArguments = functionArguments

    def getName(self):
        return self.name

    def getTaskUUID(self):
        return self.taskUUID


class RunningTask(object):
    def __init__(self, taskObject):
        self.rootObject = taskObject
        self.name = taskObject.name
        self.taskUUID = taskObject.taskUUID
        self.type = taskObject.type
        self.executionTimeObject = TimeObj(*taskObject.executionTime)
        self.functionToRun = taskObject.functionToRun
        self.functionArguments = taskObject.functionArguments
        self.runningThread = threading.Thread(target=self.functionToRun, args=self.functionArguments)
        self.comparisonBy = self.executionTimeObject.getComparsionBy()
        self.amountExecutions = 0
        self.lastActivateTime = time.localtime()
        self.nextTimeElement = self.getNextTimeElement()
        self.functionList = FunctionCollection()

    def getNextTimeElement(self):
        lastCheckedValue = 0
        i = 0
        for element in TimeObj.HEX_TIME_VALUES:
            if element & self.comparisonBy:
                lastCheckedValue = i
            i += 1
        return lastCheckedValue

    def isNextAppearance(self):
        valueForComparison = 5-self.nextTimeElement-1
        if self.lastActivateTime[valueForComparison] != time.localtime()[valueForComparison]:
            return True
        return False

    def execute(self):
        if self.isNextAppearance() and self.isExpiredTime():
            if not self.runningThread.is_alive():
                targetFunction = self.functionList.getFunctionToExecute(self.functionToRun)
                self.runningThread = threading.Thread(target=targetFunction, args=(self.functionArguments,))
                self.runningThread.start()
                self.amountExecutions += 1
            else:
                logger.warning("Wątek od zadania \"" + self.name + "\" jest ciągle uruchomiony!")
            self.lastActivateTime = time.localtime()
            return True
        return False

    def isExpiredTime(self):
        timeToCompare = time.localtime()
        return self.executionTimeObject.isExpiredToTime(timeToCompare, self.comparisonBy)

    def isToRemove(self):
        if self.type == 1 and self.amountExecutions > 0:
            return True
        return False


class TimeObj(object):
    YEAR = 0x1
    MON = 0x2
    DAY = 0x4
    HOUR = 0x8
    MIN = 0x10
    SEC = 0x20
    WDAY = 0x40
    HEX_TIME_VALUES = (SEC, MIN, HOUR, DAY, MON, YEAR, WDAY)

    def __init__(self, *args, **kwargs):
        self.comparisonList = [0x0] * 7
        startValues = [0, 0, 0, 1, 1, 1970]
        i = 0
        while i < len(args):
            startValues[i] = args[i]
            self.comparisonList[i] = self.HEX_TIME_VALUES[i]
            i += 1

        forKwargs = collections.OrderedDict((("sec", None),
                                            ("min", None),
                                            ("hour", None),
                                            ("day", None),
                                            ("mon", None),
                                            ("year", None)))
        for key, value in kwargs.items():
            forKwargs[key] = value
            keyNumber = (list(forKwargs.keys()).index(key))
            self.comparisonList[keyNumber] = self.HEX_TIME_VALUES[keyNumber]

        i = 0
        for element in forKwargs.values():
            if element:
                startValues[i] = element
            i += 1

        maxAndMin = ((0, 61), (0, 59), (0, 23), (1, 31), (1, 12), (1970, 2999))
        i = 0
        while i < 6:
            if startValues[i] < maxAndMin[i][0] or startValues[i] > maxAndMin[i][1]:
                raise NameError('OutOfRange')
            i += 1

        resultAsString = " ".join(map(str, startValues))

        self.time = time.strptime(resultAsString, '%S %M %H %d %m %Y')

    def getTime(self):
        return self.time

    def getComparsionBy(self):
        return sum(self.comparisonList)

    def isExpiredToTime(self, timeToCompare, comparisonBy): #TODO trzeba to naprawić :/
        result = True
        if self.SEC & comparisonBy and self.time[5] != timeToCompare[5]:
                if self.time[5] < timeToCompare[5]:
                    result = True
                else:
                    result = False
        if self.MIN & comparisonBy and self.time[4] != timeToCompare[4]:
                if self.time[4] < timeToCompare[4]:
                    result = True
                else:
                    result = False
        if self.HOUR & comparisonBy and self.time[3] != timeToCompare[3]:
                if self.time[3] < timeToCompare[3]:
                    result = True
                else:
                    result = False
        if self.DAY & comparisonBy and self.time[2] != timeToCompare[2]:
                if self.time[2] < timeToCompare[2]:
                    result = True
                else:
                    result = False
        if self.MON & comparisonBy and self.time[1] != timeToCompare[1]:
                if self.time[1] < timeToCompare[1]:
                    result = True
                else:
                    result = False
        if self.YEAR & comparisonBy and self.time[0] != timeToCompare[0]:
                if self.time[0] < timeToCompare[0]:
                    result = True
                else:
                    result = False
        if self.WDAY & comparisonBy and self.time[6] != timeToCompare[6]:
                if self.time[6] < timeToCompare[6]:
                    result = True
                else:
                    result = False
        return result

#test = TimeObj(time.strptime("05 09 2017 21:31:15",'%d %m %Y %H:%M:%S'))
#print(test.isExpired(test.MIN + test.SEC))

# def drukuj(element):
#     print(time.strftime("%H:%M:%S", time.gmtime()))
#     print("Test druku. Wiadomosc: "+ element)
#     time.sleep(10)
#
# harmonogram = Scheduler()
# wykonanie = TimeObj(10)
# harmonogram.newTask("drukowanie", 1, wykonanie, drukuj, ("drukuj mnie",))
# harmonogram.start()
# time.sleep(2)
# print(harmonogram.getAllInfoAsList())
# time.sleep(300)
# harmonogram.stop()

#TODO: dodać obsługe dnia tygodnia WDAY
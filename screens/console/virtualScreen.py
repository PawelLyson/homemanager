import time
import threading
import unicodedata


class ScreenConsole(object):
    def __init__(self, lines):
        self.isRunning = True
        self.linesAmount = lines
        self.linesText = [""] * self.linesAmount
        self.textThread = threading.Thread(target=self.refreshingScreen)
        self.backlightON = True
        self.startTime = time.strftime("%H:%M:%S", time.localtime())

    def start(self):
        self.isRunning = True
        self.textThread.start()
        return "Start screen"

    def stop(self):
        self.isRunning = False
        return "Stop screen"

    def setBacklight(self, setON):
        self.backlightON = setON

    def backlightToggle(self):
        self.backlightON = not self.backlightON
        return "Backlight toggled"

    def isBacklightON(self):
        return self.backlightON

    def setText(self, text, line):
        self.linesText[line] = text

    def setAllText(self, listText):
        self.linesText = listText[:]

    def refreshingScreen(self):
        while self.isRunning:
            print("\n\n\n\n\n\n\n\n\n\n\n\n")
            print(self.startTime)
            if self.backlightON:
                print("Backlight ON")
            else:
                print("Backlight OFF")
            print("LCD SCREEN " + time.strftime("%H:%M:%S", time.localtime()))
            print("------------------------")
            for i in range(self.linesAmount):
                print(self.convertToString(self.linesText[i]))
            print("------------------------")
            time.sleep(2)

    @staticmethod
    def convertToString(data):
        text = ""
        if isinstance(data, (list, tuple)):
            for element in data:
                if callable(element):
                    text += str(element())
                else:
                    text += str(element)
        elif callable(data):
            text = str(data())
        else:
            text = str(data)
        return unicodedata.normalize('NFKD', text).encode('ascii', 'ignore').decode("utf-8")

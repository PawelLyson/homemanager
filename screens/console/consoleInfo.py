import logging


def createLogger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler('log.log')
    handler.setLevel(logging.DEBUG)
    consoleLog = logging.StreamHandler()
    consoleLog.setLevel(logging.DEBUG)
    formatter = logging.Formatter(fmt='%(asctime)s [%(levelname)s]: %(message)s', datefmt='%y.%m.%d %H:%M:%S')
    handler.setFormatter(formatter)
    consoleLog.setFormatter(formatter)
    logger.addHandler(handler)
    logger.addHandler(consoleLog)
    return logger
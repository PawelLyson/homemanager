import smbus
import time
import threading
import unicodedata


class LCDi2c(object):
    LCDDATA = 1
    LCDCMD = 0
    PULSE = 0.0005
    DELAY = 0.0005
    ENABLE = 0b00000100
    BACKLIGHT = 0x08

    LINES = (0x80, 0xC0, 0x94, 0xD4)  # for lines

    def __init__(self, i2cAddr, lcdWidth, lines, i2cInterface = 1):
        self.i2cAddr = i2cAddr
        self.lcdWidth = lcdWidth
        self.lines = lines
        self.bus = smbus.SMBus(i2cInterface)
        self.lcdByte(0x33, self.LCDCMD)  # 110011 Initialise
        self.lcdByte(0x32, self.LCDCMD)  # 110010 Initialise
        self.lcdByte(0x06, self.LCDCMD)  # 000110 Cursor move direction
        self.lcdByte(0x0C, self.LCDCMD)  # 001100 Display On,Cursor Off, Blink Off
        self.lcdByte(0x28, self.LCDCMD)  # 101000 Data length, number of lines, font size
        self.lcdByte(0x01, self.LCDCMD)  # 000001 Clear display
        time.sleep(self.DELAY)

    def lcdByte(self, bits, mode):
        bitsHigh = mode | (bits & 0xF0) | self.BACKLIGHT
        bitsLow = mode | ((bits << 4) & 0xF0) | self.BACKLIGHT

        # High bits
        self.bus.write_byte(self.i2cAddr, bitsHigh)
        self.lcdToggle(bitsHigh)

        # Low bits
        self.bus.write_byte(self.i2cAddr, bitsLow)
        self.lcdToggle(bitsLow)

    def lcdToggle(self, bits):
        # Toggle enable
        time.sleep(self.DELAY)
        self.bus.write_byte(self.i2cAddr, (bits | self.ENABLE))
        time.sleep(self.PULSE)
        self.bus.write_byte(self.i2cAddr, (bits & ~self.ENABLE))
        time.sleep(self.DELAY)

    def backlightToggle(self):
        if self.BACKLIGHT == 0x08:
            self.BACKLIGHT = 0x00
            return "Backlight OFF"
        else:
            self.BACKLIGHT = 0x08
            return "backlight ON"

    def writeLine(self, text, line):  # Send string to display
        message = text.ljust(self.lcdWidth, " ")

        self.lcdByte(self.LINES[line], self.LCDCMD)

        for i in range(self.lcdWidth):
            self.lcdByte(ord(message[i]), self.LCDDATA)

    def getLCDWith(self):
        return self.lcdWidth

    def setBacklight(self, setON):
        if setON:
            self.BACKLIGHT = 0x08
        else:
            self.BACKLIGHT = 0x00

    def isBacklightON(self):
        if self.BACKLIGHT == 0x08:
            return True
        return False

class LCDi2cMarquee(LCDi2c):
    def __init__(self, i2cAddr, lcdWidth, lines, i2cInterface = 1):
        super(LCDi2cMarquee,self).__init__(i2cAddr, lcdWidth, lines, i2cInterface)
        self.sleepTime = 0.5
        self.linesText = ["", "", "", ""]
        self.isRunning = True
        self.textThread = None
        self.moveText = [0, 0, 0, 0]
        self.lock = threading.Lock()

    def setText(self, text, line):
        self.lock.acquire()
        self.moveText[line] = 0
        self.linesText[line] = text
        self.lock.release()

    def setAllText(self, listText):
        self.lock.acquire()
        self.moveText = [0, 0, 0, 0]
        self.linesText = listText[:]
        self.lock.release()

    def marqueeText(self):
        while self.isRunning:
            self.lock.acquire()
            for i in range(4):
                text = self.convertToString(self.linesText[i])
                preparedText = text[self.moveText[i]:len(text)] + text[0:self.moveText[i]]
                self.writeLine(preparedText, i)
                if len(text) > self.lcdWidth:
                    self.moveText[i] += 1
                    if self.moveText[i] > len(text) - 2:
                        self.moveText[i] = 0
            self.lock.release()
            time.sleep(self.sleepTime)

    @staticmethod
    def convertToString(data):
        text = ""
        if isinstance(data, (list, tuple)):
            for element in data:
                if callable(element):
                    text += str(element())
                else:
                    text += str(element)
        elif callable(data):
            text = str(data())
        else:
            text = str(data)
        return unicodedata.normalize('NFKD', text).encode('ascii', 'ignore').decode("utf-8")

    def start(self):
        self.isRunning = True
        self.textThread = threading.Thread(target=self.marqueeText)
        self.textThread.start()
        return "LCD started"

    def stop(self):
        time.sleep(1)
        self.lcdByte(0x08, self.LCDCMD)
        self.isRunning = False
        return "LCD stopped"


if __name__ == "__main__":
    print("This is module. Please import")

import importlib.util
if importlib.util.find_spec("smbus") is not None:
    import screens.lcDriverMarquee.lcd_i2c as lcdI2C
import screens.console.virtualScreen
import screens.console.consoleInfo


class OutsideScreen(object):
    def __init__(self):
        logger = screens.console.consoleInfo.createLogger(__name__+"screens")
        haveSmbus = importlib.util.find_spec("smbus")
        self.isLCD = haveSmbus is not None
        self.screen = None
        if self.isLCD:
            logger.debug("Uruchomiono LCD i2c")
            self.screen = lcdI2C.LCDi2cMarquee(0x27, 20, 4)
        else:
            logger.debug("Uruchomiono virtualny LCD")
            self.screen = screens.console.virtualScreen.ScreenConsole(4)

    def startScreen(self):
        self.screen.start()

    def setOneLine(self, text, line):
        self.screen.setText(text, line)

    def setAllLines(self, listText):
        self.screen.setAllText(listText)

    def stopScreen(self):
        self.screen.stop()

    def backlightToggle(self):
        self.screen.backlightToggle()

    def setBacklight(self, setON):
        self.screen.setBacklight(setON)

    def isBacklightON(self):
        return self.screen.isBacklightON()

    def getListMethods(self):
        return self.backlightToggle, self.setBacklight, self.isBacklightON

    #TODO: metoda powinna mieć 3 dodatkowe (sub)atrybuty: nazwa pokazywana, czy zwraca wartość i jaki rodzaj, argumenty(nazwa,typ)

